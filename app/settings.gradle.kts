plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.5.0"
}
rootProject.name = "app"

include(":library")
project(":library").projectDir = File("../library")