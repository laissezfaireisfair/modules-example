package and.modules.model

class Calculator {
    companion object {
        fun compute(arg: Int) = arg * arg
    }
}