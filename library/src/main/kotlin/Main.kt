package and.modules

import and.modules.model.Calculator

fun main() = with(Calculator.compute(30)) { println("$this") }