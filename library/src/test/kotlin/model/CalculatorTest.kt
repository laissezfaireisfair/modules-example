package model

import and.modules.model.Calculator
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CalculatorTest {
    @Test
    fun computeTest() {
        (0..100).forEach { assertEquals(it * it, Calculator.compute(it)) }
    }
}